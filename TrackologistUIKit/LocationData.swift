//
//  LocationData.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 10.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class LocationData: Codable {
    
    static let dateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      formatter.timeStyle = .medium
      return formatter
    }()
    
    let latitude: Double
    let longitude: Double
    let date: Date
    let dateString: String
    let description: String
    
    var coordinates: CLLocationCoordinate2D {
      return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(_ location: CLLocationCoordinate2D, date: Date, descriptionString: String) {
      latitude =  location.latitude
      longitude =  location.longitude
      self.date = date
      dateString = LocationData.dateFormatter.string(from: date)
      description = descriptionString
    }
    
    convenience init(visit: CLVisit, descriptionString: String) {
      self.init(visit.coordinate, date: visit.arrivalDate, descriptionString: descriptionString)
    }
}
