//
//  PlaceTableViewController.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 11.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData


struct cellData {
    var opened = Bool()
    var title = String()
    var date = String()
    
}
class PlaceTableViewController: UITableViewController {
    
    var tableViewData = [cellData] ()
    var locations = LocationStorage.shared.locations
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(
        self,
        selector: #selector(newLocationAdded(_:)),
        name: .newLocationSaved,
        object: nil)
        tableView.reloadData()
        
        
    }
    @objc func newLocationAdded(_ notification: Notification) {
      tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return tableViewData.count
        return LocationStorage.shared.locations.count
//        if tableViewData[section].opened == true {
//            return tableViewData[section].date.count
//
//        } else {
//            return 1
//        }
     }

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return LocationStorage.shared.locations.count
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath)
        let location = LocationStorage.shared.locations[indexPath.row]
        let delimeter: Character = ","
        let descript = location.description
        let address = descript.split(separator: delimeter)
//        tableViewData = [cellData(opened: false, title: String(address[0]), date: location.dateString)]
//        if indexPath.row == 0 {
//            cell.textLabel?.text = tableViewData[indexPath.section].title
//             return cell
//        } else {
//            cell.textLabel?.text = tableViewData[indexPath.section].date
//            return cell
//        }
        cell.textLabel?.numberOfLines = 3
        cell.textLabel?.text = String(address[0])
        cell.detailTextLabel?.text = location.dateString
        return cell
       
    }
    
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if locations[indexPath.section].opened == true {
//            tableViewData[indexPath.section].opened = false
//            let sections = IndexSet.init(integer: indexPath.section)
//            tableView.reloadSections(sections, with: .none)
//        } else {
//            tableViewData[indexPath.section].opened = true
//            let sections = IndexSet.init(integer: indexPath.section)
//            tableView.reloadSections(sections, with: .none)
//        }
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            let location = LocationStorage.shared.locations[indexPath.row]
            let destinationView = PlaceDetailViewController()
//            let newTableview = destinationView.tableView(tableView, cellForRowAt: indexPath)
            navigationController?.pushViewController(destinationView, animated: true)
//            let cell = newTableview.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
//            let delimeter: Character = ","
//            let descript = location.description
//            let address = descript.split(separator: delimeter)
//            cell.textLabel?.numberOfLines = 3
//            cell.textLabel?.text = String(address[0])
//            cell.detailTextLabel?.text = location.dateString
            
        
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    @objc override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
  
        if (editingStyle == .delete && indexPath.section == 2) {
            self.locations.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        
    }
   
}
