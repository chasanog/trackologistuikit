//
//  AppDelegate.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 10.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    let center = UNUserNotificationCenter.current()
    let locationManager = CLLocationManager()
    
    static let geocoder = CLGeocoder()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        center.requestAuthorization(options: [.alert, .sound]){
            granted, error in
        }
        locationManager.requestAlwaysAuthorization()
        //locationManager.startMonitoringVisits()
        //locationManager.delegate = self
        
        return true
    }
    func saveContext() {
        
    }
    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "Your Model File Name")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {

                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }()


}
extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
      // create CLLocation from the coordinates of CLVisit
      let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)
      
      AppDelegate.geocoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
        if let place = placemarks?.first {
          let description = "\(place)"
          self.newVisitReceived(visit, description: description)
        }
      }
      // Get location description
    }

    func newVisitReceived(_ visit: CLVisit, description: String) {
        let location = LocationData(visit: visit, descriptionString: description)
        LocationStorage.shared.saveLocationOnDisk(location)
        let content = UNMutableNotificationContent()
        let delimeter: Character = ","
        let descript = location.description
        let address = descript.split(separator: delimeter)
        content.title = "Entered Location"
        content.body = String(address[0])
        content.sound = .default
          
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: location.dateString, content: content, trigger: trigger)
          
        center.add(request, withCompletionHandler: nil)
      // Save location to disk
    }
  }

