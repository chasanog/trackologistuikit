//
//  PlaceDetailViewController.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 15.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import UIKit


class PlaceDetailViewController: UITableViewController {
    var locations = LocationStorage.shared.locations
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
            let location = LocationStorage.shared.locations[indexPath.row]
            let delimeter: Character = ","
            let descript = location.description
            let address = descript.split(separator: delimeter)
    //        tableViewData = [cellData(opened: false, title: String(address[0]), date: location.dateString)]
    //        if indexPath.row == 0 {
    //            cell.textLabel?.text = tableViewData[indexPath.section].title
    //             return cell
    //        } else {
    //            cell.textLabel?.text = tableViewData[indexPath.section].date
    //            return cell
    //        }
            cell.textLabel?.numberOfLines = 3
            cell.textLabel?.text = String(address[0])
            cell.detailTextLabel?.text = location.dateString
            return cell
           
        }
}
