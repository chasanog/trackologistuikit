//
//  MapViewController.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 11.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import Foundation
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.userTrackingMode = .follow
            
        let annotations = LocationStorage.shared.locations.map { annotationForLocation($0) }
        mapView.addAnnotations(annotations)
        
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(newLocationAdded(_:)),
        name: .newLocationSaved,
        object: nil)
        


    }
    
    @IBAction func mapCenterButton(_ sender: UIButton) {
        self.mapView.setCenter(self.mapView.userLocation.coordinate, animated: true)
        
    }
    
    @IBAction func addItemPressed(_ sender: Any) {
        guard let currentLocation = mapView.userLocation.location else {
          return
        }

        LocationStorage.shared.saveCLLocationToDisk(currentLocation)
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
        }

        let alert = UIAlertController(title: "Location Added", message: "Current location has been added to the Places Tab!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
     creates a pin for every location
     */
    
    func annotationForLocation(_ location: LocationData) -> MKAnnotation {
      let annotation = MKPointAnnotation()
      annotation.title = location.dateString
      annotation.coordinate = location.coordinates
      return annotation
    }
    
    @objc func newLocationAdded(_ notification: Notification) {
      guard let location = notification.userInfo?["location"] as? LocationData else {
        return
      }

      let annotation = annotationForLocation(location)
      mapView.addAnnotation(annotation)
    }


    
}
