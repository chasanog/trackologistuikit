//
//  LocationStorage.swift
//  TrackologistUIKit
//
//  Created by Cihan Hasanoglu on 10.12.19.
//  Copyright © 2019 Cihan Hasanoglu. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import CoreData

class LocationStorage {
    static let shared = LocationStorage()
    
    private(set) var locations: [LocationData]
    
    // two write and read to/from the disk
    private let fileManager: FileManager
    private let documentsURL: URL
    
    
    /*
     Get URLs for all files in the Documents folder, Skip .DS_Store file, Read data from file, Decode raw data into Location objects, Sort Location by date.
     */
    init() {
      let fileManager = FileManager.default
      documentsURL = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
      self.fileManager = fileManager
      let jsonDecoder = JSONDecoder()

      // 1
      let locationFilesURLs = try! fileManager
        .contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
      locations = locationFilesURLs.compactMap { url -> LocationData? in
        // 2
        guard !url.absoluteString.contains(".DS_Store") else {
          return nil
        }
        // 3
        guard let data = try? Data(contentsOf: url) else {
          return nil
        }
        // 4
        return try? jsonDecoder.decode(LocationData.self, from: data)
        // 5
        }.sorted(by: { $0.date > $1.date })

    }
    
    func saveLocationOnDisk(_ location: LocationData) {
      let encoder = JSONEncoder()
        let timestamp = location.date.timeIntervalSince1970
//        let delimeter: Character = ","
//        let descript = location.description
//        let address = descript.split(separator: delimeter)

        let fileURL = documentsURL.appendingPathComponent("\(timestamp)")

        let data = try! encoder.encode(location)

        try! data.write(to: fileURL)

        locations.append(location)
    }
    
    /*
     create Location object from clLocation with the current location description and date from geocoder.
     */
    func saveCLLocationToDisk(_ clLocation: CLLocation) {
      let currentDate = Date()
      AppDelegate.geocoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
        if let place = placemarks?.first {
          let location = LocationData(clLocation.coordinate, date: currentDate, descriptionString: "\(place)")
          self.saveLocationOnDisk(location)
          NotificationCenter.default.post(name: .newLocationSaved, object: self, userInfo: ["location": location])
        }
      }
    }
    
    func removeLocationFromList(_ location: LocationData) {
        self.locations.removeAll()
        
//        locations.removeAll(where: { (location) -> Bool in
//            return true
//        })
        
        
    }

}

extension Notification.Name {
  static let newLocationSaved = Notification.Name("newLocationSaved")
}

